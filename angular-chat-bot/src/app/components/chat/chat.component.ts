import { AfterViewInit, Component } from '@angular/core';
import { Interactions } from 'aws-amplify';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements AfterViewInit {

  userInput: string;
  mainChatMessage = [
    {
      type: 'bot',
      message: {
        messageFormat: 'PlainText',
        message: 'Hi. Let us help you get the information you need.'
      }
    },
    {
      type: 'bot',
      message: {
        messageFormat: 'PlainText',
        message: 'Are you looking to…'
      }
    }
  ];
  messageList: any = [...this.mainChatMessage];

  constructor() { }

  ngAfterViewInit(): void {
    this.initializeMainChat();
  }

  async initializeMainChat(): Promise<void> {
    this.messageList.push({
      type: 'loading'
    });
    const message = await Interactions.send('GetAdvise', 'Hi');
    this.messageList.pop();
    this.messageList.push({
      type: 'bot',
      message
    });
  }

  async sendText(userInputForm): Promise<void> {
    if (userInputForm.valid) {
      const userInput = userInputForm.value.userInput;
      userInputForm.reset();
      this.sendMessage(userInput);
    }
  }

  async sendMessage(userInput): Promise<void> {
    this.messageList.push({
      type: 'user',
      message: userInput
    });
    this.messageList.push({
      type: 'loading'
    });
    const message = await Interactions.send('GetAdvise', userInput);
    this.messageList.pop();
    this.messageList.push({
      type: 'bot',
      message
    });
    if (message.dialogState === 'Fulfilled' && message.intentName === 'CheckPreviousWork') {
      this.messageList = this.messageList.concat(this.mainChatMessage);
      this.initializeMainChat();
    }
  }

  getMessage(message): string {
    const parsed = JSON.parse(message);
    return parsed.messages.map(msg => {
      if (msg.type === 'PlainText' || (typeof msg.value) === 'string') {
        return msg.value;
      }
      this.getMessage(msg);
    }).join('<br>');
  }

}
