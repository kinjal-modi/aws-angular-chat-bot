'use strict';
const AWS = require('aws-sdk');
const axios = require('axios');
AWS.config.update({
  region: "us-east-1"
});
const getInTouchWithInternalUser = async (event) => {
  if (event.currentIntent.slots.getInTouch === 'yes') {
    // return {
    //   "dialogAction": {
    //     "type": "ElicitSlot",
    //     "message": {
    //       "contentType": "PlainText",
    //       "content": "Great! Do you know them by name or department?"
    //     },
    //     "intentName": "SelectNameDepartment",
    //     "slots": {
    //       "selection": "null"
    //     },
    //     "slotToElicit": "selection",
    //     "responseCard": {
    //       "version": 1,
    //       "contentType": "application/vnd.amazonaws.card.generic",
    //       "genericAttachments": [{
    //         "title": "Great! Do you know them by name or department?",
    //         "subTitle": "Great! Do you know them by name or department?",
    //         "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
    //         "attachmentLinkUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
    //         "buttons": [{
    //           "text": "Yes",
    //           "value": "yes"
    //         },
    //         {
    //           "text": "No",
    //           "value": "no"
    //         }
    //         ]
    //       }]
    //     }
    //   }
    // };
    return {
      "dialogAction": {
        "type": "ElicitSlot",
        "message": {
          "contentType": "PlainText",
          "content": "Great! Do you know them by name or department?"
        },
        "intentName": "FindNameDepartment",
        "slots": {
          "value": null
        },
        "slotToElicit": "value",
        "responseCard": {
          "version": 1,
          "contentType": "application/vnd.amazonaws.card.generic",
          "genericAttachments": [{
            "title": "Great! Do you know them by name or department?",
            "subTitle": "Enter name or department value",
            "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
            "attachmentLinkUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg"
          }]
        }
      }
    }
  }
  else {
    return {
      "dialogAction": {
        "type": "ElicitSlot",
        "message": {
          "contentType": "PlainText",
          "content": "Get legal or consultancy advice"
        },
        "intentName": "GetAdvice",
        "slots": {
          "adviceSelection": null
        },
        "slotToElicit": "adviceSelection",
        "responseCard": {
          "version": 1,
          "contentType": "application/vnd.amazonaws.card.generic",
          "genericAttachments": [{
            "title": "Get legal or consultancy advice",
            "subTitle": "Get legal or consultancy advice",
            "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
            "attachmentLinkUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
            "buttons": [{
              "text": "Yes",
              "value": "yes"
            },
            {
              "text": "No",
              "value": "no"
            }
            ]
          }]
        }
      }
    }
  }
};

const selecteIntent = async (event) => {
  const data = {
    "dialogAction": {
      "type": "ElicitSlot",
      "message": {
        "contentType": "PlainText",
        "content": ""
      },
      "intentName": event.currentIntent.slots.selecteIntent,
      "slots": {},
      "slotToElicit": "",
      "responseCard": {
        "version": 1,
        "contentType": "application/vnd.amazonaws.card.generic",
        "genericAttachments": [{
          "title": "",
          "subTitle": "",
          "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
          "attachmentLinkUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
          "buttons": [{
            "text": "Yes",
            "value": "yes"
          },
          {
            "text": "No",
            "value": "no"
          }]
        }]
      }
    }
  };

  switch (event.currentIntent.slots.selecteIntent) {
    case 'WelcomeMessage': {
      data.dialogAction.message.content = 'Get in touch with a member of our team';
      data.dialogAction.slots = {
        getInTouch: null
      };
      data.dialogAction.slotToElicit = 'getInTouch';
      data.dialogAction.responseCard.genericAttachments[0].title = 'Great! Do you know them by name or department?';
      data.dialogAction.responseCard.genericAttachments[0].subTitle = 'Great! Do you know them by name or department?';
      return data;
    }
    case 'GetAdvice': {
      data.dialogAction.message.content = 'Get in touch with a member of our team';
      data.dialogAction.slots = {
        adviceSelection: null
      };
      data.dialogAction.slotToElicit = 'adviceSelection';
      data.dialogAction.responseCard.genericAttachments[0].title = 'Get legal or consultancy advice';
      data.dialogAction.responseCard.genericAttachments[0].subTitle = 'Get legal or consultancy advice';
      return data;
    }
    case 'FindSpecificContent': {
      data.dialogAction.message.content = 'Find specific content on our website';
      data.dialogAction.slots = {
        specificContent: null
      };
      data.dialogAction.slotToElicit = 'specificContent';
      data.dialogAction.responseCard.genericAttachments[0].title = 'Find specific content on our website';
      data.dialogAction.responseCard.genericAttachments[0].subTitle = 'Find specific content on our website';
      return data;
    }
    case 'ReportProblem': {
      data.dialogAction.message.content = 'Report a complaint/speak to someone about a service issue?';
      data.dialogAction.slots = {
        reportProblemSelection: null
      };
      data.dialogAction.slotToElicit = 'reportProblemSelection';
      data.dialogAction.responseCard.genericAttachments[0].title = 'Report a complaint/speak to someone about a service issue?';
      data.dialogAction.responseCard.genericAttachments[0].subTitle = 'Report a complaint/speak to someone about a service issue?';
      return data;
    }
  }
};

const allContentSearch = async (event) => {
  // todo: search in DB (event.currentIntent.slots.allContentInput)
  // TODO: need to implement api call
  const documentClient = new AWS.DynamoDB.DocumentClient({ region: "us-east-1" });
  const params = {
    TableName: "USERS",
    FilterExpression: 'contains(#name, :name) OR contains(#dept, :name)',
    ExpressionAttributeValues: { ":name": event.currentIntent.slots.allContentInput },
    ExpressionAttributeNames: {
      "#name": "name",
      "#dept": "department"
    }
  };

  try {
    console.log('users', params);
    const users = await documentClient.scan(params).promise();
    console.log('users', users);
    const attachment = [{
      "title": "We hope you've found what you're looking for.",
      "subTitle": "We hope you've found what you're looking for.",
      "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
      "attachmentLinkUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
    }, {
      "title": "Alternatively, please email BDMQueries@incegd.com for support.",
      "subTitle": "Alternatively, please email BDMQueries@incegd.com for support.",
      "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
      "attachmentLinkUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
    }, {
      "title": "Is there anything else we can help you with?",
      "subTitle": "Is there anything else we can help you with?",
      "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
      "attachmentLinkUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
      "buttons": [{
        "text": "Yes",
        "value": "yes"
      },
      {
        "text": "No",
        "value": "no"
      }]
    }];
    if (users && user.Items && users.Items.length) {
      attachment.unshift({
        "title": "Info About " + users.Items[0].name,
        "subTitle": "Name: " + users.Items[0].name,
        "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
        "attachmentLinkUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
      });
    } else {
      attachment.unshift({
        "title": "Info About " + event.currentIntent.slots.allContentInput,
        "subTitle": "Name: " + event.currentIntent.slots.allContentInput,
        "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
        "attachmentLinkUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
      });
    }
    return {
      "dialogAction": {
        "type": "ElicitSlot",
        "message": {
          "contentType": "PlainText",
          "content": "We found Info about " + event.currentIntent.slots.allContentInput
        },
        "intentName": "NeedOtherHelp",
        "slots": {
          "otherHelpInput": null
        },
        "slotToElicit": "otherHelpInput",
        "responseCard": {
          "version": 1,
          "contentType": "application/vnd.amazonaws.card.generic",
          "genericAttachments": attachment
        }
      }
    };
  } catch (error) {
    console.log(error);
  }
};

const checkForPreviousWork = async (event) => {
  try {
    if (event.currentIntent.slots.phoneEmailSelection === 'phone') {
      if (event.currentIntent.slots.phoneNumber) {
        if (event.currentIntent.slots.timeToCall) {
          return handleConfirmation(event);
        }
        return {
          "dialogAction": {
            "type": "ElicitSlot",
            "message": {
              "contentType": "PlainText",
              "content": "What is the best time to call you?"
            },
            "intentName": "CheckPreviousWork",
            "slots": {
              ...event.currentIntent.slots
            },
            "slotToElicit": "timeToCall"
          }
        }
      }
      return {
        "dialogAction": {
          "type": "ElicitSlot",
          "message": {
            "contentType": "PlainText",
            "content": "Please enter phone number"
          },
          "intentName": "CheckPreviousWork",
          "slots": {
            ...event.currentIntent.slots
          },
          "slotToElicit": "phoneNumber"
        }
      }
    }
    else {
      if (event.currentIntent.slots.email) {
        return handleConfirmation(event);
      }
      return {
        "dialogAction": {
          "type": "ElicitSlot",
          "message": {
            "contentType": "PlainText",
            "content": "Please enter email"
          },
          "intentName": "CheckPreviousWork",
          "slots": {
            ...event.currentIntent.slots
          },
          "slotToElicit": "email"
        }
      }
    }
  }
  catch (error) {
    console.log('error in check for previos work', error);
  }
};

const contactShipping = async (event) => {
  if (event.currentIntent.slots.contactShipping === 'yes') {
    // need to change
    return {
      "dialogAction": {
        "type": "ElicitSlot",
        "message": {
          "contentType": "PlainText",
          "content": "Emergency response page"
        },
        "intentName": "IntentSelection",
        "slots": {
          "selecteIntent": null
        },
        "slotToElicit": "selecteIntent",
        "responseCard": {
          "version": 1,
          "contentType": "application/vnd.amazonaws.card.generic",
          "genericAttachments": [{
            "title": "Emergency response page",
            "subTitle": "Emergency response page",
            "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
            "attachmentLinkUrl": "https://www.google.com/",
          }, {
            "title": "We hope this information was useful.",
            "subTitle": "We hope this information was useful.",
            "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
            "attachmentLinkUrl": "https://www.google.com/",
          }, {
            "title": "Is there anything else we can help you with?",
            "subTitle": "Is there anything else we can help you with?",
            "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
            "attachmentLinkUrl": "https://www.google.com/",
          }, {
            "title": "Are you also looking to…",
            "subTitle": "If you're unsure, can you tell us what you're looking for?",
            "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
            "attachmentLinkUrl": "https://www.google.com/",
            "buttons": [{
              "text": "Get in touch with a member of our team?",
              "value": "WelcomeMessage"
            }, {
              "text": "Get legal or consultancy advice?",
              "value": "GetAdvice"
            }, {
              "text": "Find specific content on our website",
              "value": "FindSpecificContent"
            }, {
              "text": "Report a complaint/speak to someone about a service issue?",
              "value": "ReportProblem"
            },
            ]
          }]
        }
      }
    };
  }
  else {
    return {
      "dialogAction": {
        "type": "ElicitSlot",
        "message": {
          "contentType": "PlainText",
          "content": "Report a complaint/speak to someone about a service issue?"
        },
        "intentName": "ReportProblem",
        "slots": {
          "reportProblemSelection": null
        },
        "slotToElicit": "reportProblemSelection",
        "responseCard": {
          "version": 1,
          "contentType": "application/vnd.amazonaws.card.generic",
          "genericAttachments": [{
            "title": "Report a complaint/speak to someone about a service issue?",
            "subTitle": "Report a complaint/speak to someone about a service issue?",
            "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
            "attachmentLinkUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
            "buttons": [{
              "text": "Yes",
              "value": "yes"
            },
            {
              "text": "No",
              "value": "no"
            }
            ]
          }]
        }
      }
    }
  }
};

const searchNameDepartment = async (event) => {
  // TODO: need to implement api call
  const documentClient = new AWS.DynamoDB.DocumentClient({ region: "us-east-1" });
  const params = {
    TableName: "USERS",
    FilterExpression: 'contains(#name, :name) OR contains(#dept, :name)',
    ExpressionAttributeValues: { ":name": event.currentIntent.slots.value },
    ExpressionAttributeNames: {
      "#name": "name",
      "#dept": "department"
    }
  };

  try {
    console.log('users', params);
    const users = await documentClient.scan(params).promise();
    console.log('users', users);
    const attachment = [{
      "title": "Was this information useful?",
      "subTitle": "Was this information useful?",
      "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
      "attachmentLinkUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
      "buttons": [{
        "text": "Yes",
        "value": "yes"
      },
      {
        "text": "No",
        "value": "no"
      }]
    }];
    if (users.Items && users.Items.length) {
      attachment.unshift({
        "title": "Info About " + event.currentIntent.slots.value,
        "subTitle": "Name: " + event.currentIntent.slots.value,
        "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
        "attachmentLinkUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
      });
    } else {
      attachment.unshift({
        "title": "No, Info About " + event.currentIntent.slots.value,
        "subTitle": "unable to find info about " + event.currentIntent.slots.value,
        "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
        "attachmentLinkUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
      });
    }

    return {
      "dialogAction": {
        "type": "ElicitSlot",
        "message": {
          "contentType": "PlainText",
          "content": "We found Info about " + event.currentIntent.slots.value
        },
        "intentName": "UsefullContent",
        "slots": {
          "isContentUsefull": null
        },
        "slotToElicit": "isContentUsefull",
        "responseCard": {
          "version": 1,
          "contentType": "application/vnd.amazonaws.card.generic",
          "genericAttachments": attachment
        }
      }
    };
  } catch (error) {
    console.log(error);
  }
};

const findSpecific = async (event) => {
  if (event.currentIntent.slots.specificContent === 'yes') {
    return {
      "dialogAction": {
        "type": "ElicitSlot",
        "message": {
          "contentType": "PlainText",
          "content": "Hello, what information can we help you find?"
        },
        "intentName": "SpecificContentSearch",
        "slots": {
          "userInput": null,
          "allSearch": "no"
        },
        "slotToElicit": "userInput"
      }
    };
  }
  else {
    return {
      "dialogAction": {
        "type": "ElicitSlot",
        "message": {
          "contentType": "PlainText",
          "content": "Contact our emergency response team (Shipping)"
        },
        "intentName": "ContactShippingTeam",
        "slots": {
          "contactShipping": null
        },
        "slotToElicit": "contactShipping",
        "responseCard": {
          "version": 1,
          "contentType": "application/vnd.amazonaws.card.generic",
          "genericAttachments": [{
            "title": "Contact our emergency response team (Shipping)",
            "subTitle": "Contact our emergency response team (Shipping)",
            "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
            "attachmentLinkUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
            "buttons": [{
              "text": "Yes",
              "value": "yes"
            },
            {
              "text": "No",
              "value": "no"
            }
            ]
          }]
        }
      }
    }
  }
};

const adviceSelection = async (event) => {
  if (event.currentIntent.slots.adviceSelection === 'yes') {
    return checkPreviousWork(event);
  }
  else {
    return {
      "dialogAction": {
        "type": "ElicitSlot",
        "message": {
          "contentType": "PlainText",
          "content": "Find specific content on our website"
        },
        "intentName": "FindSpecificContent",
        "slots": {
          "specificContent": null
        },
        "slotToElicit": "specificContent",
        "responseCard": {
          "version": 1,
          "contentType": "application/vnd.amazonaws.card.generic",
          "genericAttachments": [{
            "title": "Find specific content on our website",
            "subTitle": "Find specific content on our website",
            "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
            "attachmentLinkUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
            "buttons": [{
              "text": "Yes",
              "value": "yes"
            },
            {
              "text": "No",
              "value": "no"
            }
            ]
          }]
        }
      }
    }
  }
};

const needLegalInfo = async (event) => {
  if (event.currentIntent.slots.needLegalInfo === 'yes') {
    return checkPreviousWork(event);
  }
  else {
    return sendFullfilledMessage('Great! Thank you for getting in touch.', 'PlainText');
  }
};

const needOtherHelp = async (event) => {
  if (event.currentIntent.slots.otherHelpInput === 'yes') {
    return sendLegalInfo(event);
  }
  else {
    return sendFullfilledMessage('Great! Thank you for getting in touch.', 'PlainText');
  }
};

const reportProblem = async (event) => {
  if (event.currentIntent.slots.reportProblemSelection === 'yes') {
    const customMessage = [{
      type: 'PlainText',
      value: 'Okay. You can get in touch with us directly at @email'
    }, {
      type: 'PlainText',
      value: 'Thank you for getting in touch.'
    }];
    return sendFullfilledMessage(JSON.stringify({ messages: customMessage }), 'CustomPayload');
  }
  else {
    return {
      "dialogAction": {
        "type": "ElicitSlot",
        "message": {
          "contentType": "PlainText",
          "content": "Get in touch with a member of our team?"
        },
        "intentName": "WelcomeMessage",
        "slots": {
          "getInTouch": null
        },
        "slotToElicit": "getInTouch",
        "responseCard": {
          "version": 1,
          "contentType": "application/vnd.amazonaws.card.generic",
          "genericAttachments": [{
            "title": "Get in touch with a member of our team?",
            "subTitle": "Get in touch with a member of our team?",
            "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
            "attachmentLinkUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
            "buttons": [{
              "text": "Yes",
              "value": "yes"
            },
            {
              "text": "No",
              "value": "no"
            }
            ]
          }]
        }
      }
    }
  }
};

const NameDepartmentSelection = async (event) => {
  if (event.currentIntent.slots.selection === 'yes') {
    return {
      "dialogAction": {
        "type": "ElicitSlot",
        "message": {
          "contentType": "PlainText",
          "content": "Enter name or department value"
        },
        "intentName": "FindNameDepartment",
        "slots": {
          "value": null
        },
        "slotToElicit": "value",
        "responseCard": {
          "version": 1,
          "contentType": "application/vnd.amazonaws.card.generic",
          "genericAttachments": [{
            "title": "Enter name or department value",
            "subTitle": "Enter name or department value",
            "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
            "attachmentLinkUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg"
          }]
        }
      }
    }
  } else {
    return {
      "dialogAction": {
        "type": "ElicitSlot",
        "message": {
          "contentType": "PlainText",
          "content": "If you're unsure, can you tell us what you're looking for?"
        },
        "intentName": "SpecificContentSearch",
        "slots": {
          "userInput": null,
          "allSearch": "yes"
        },
        "slotToElicit": "userInput",
        "responseCard": {
          "version": 1,
          "contentType": "application/vnd.amazonaws.card.generic",
          "genericAttachments": [{
            "title": "If you're unsure, can you tell us what you're looking for?",
            "subTitle": "If you're unsure, can you tell us what you're looking for?",
            "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
            "attachmentLinkUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg"
          }]
        }
      }
    };
  }
};

const specificContentSearch = async (event) => {
  if (event.currentIntent.slots.allSearch === 'yes') {
    // TODO: need to fetch data for userInput
    return {
      "dialogAction": {
        "type": "ElicitSlot",
        "message": {
          "contentType": "PlainText",
          "content": "We found Info about " + event.currentIntent.slots.userInput
        },
        "intentName": "NeedOtherHelp",
        "slots": {
          "otherHelpInput": null
        },
        "slotToElicit": "otherHelpInput",
        "responseCard": {
          "version": 1,
          "contentType": "application/vnd.amazonaws.card.generic",
          "genericAttachments": [{
            "title": "We hope you've found what you're looking for.",
            "subTitle": "We hope you've found what you're looking for.",
            "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
            "attachmentLinkUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
          }, {
            "title": "Alternatively, please email BDMQueries@incegd.com for support.",
            "subTitle": "Alternatively, please email BDMQueries@incegd.com for support.",
            "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
            "attachmentLinkUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
          }, {
            "title": "Is there anything else we can help you with?",
            "subTitle": "Is there anything else we can help you with?",
            "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
            "attachmentLinkUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
            "buttons": [{
              "text": "Yes",
              "value": "yes"
            },
            {
              "text": "No",
              "value": "no"
            }
            ]
          }]
        }
      }
    };
  }
  else {
    return {
      "dialogAction": {
        "type": "ElicitSlot",
        "message": {
          "contentType": "PlainText",
          "content": "We found Info about " + event.currentIntent.slots.userInput
        },
        "intentName": "UsefullContent",
        "slots": {
          "isContentUsefull": null
        },
        "slotToElicit": "isContentUsefull",
        "responseCard": {
          "version": 1,
          "contentType": "application/vnd.amazonaws.card.generic",
          "genericAttachments": [{
            "title": "Was this information useful?",
            "subTitle": "Was this information useful?",
            "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
            "attachmentLinkUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
            "buttons": [{
              "text": "Yes",
              "value": "yes"
            },
            {
              "text": "No",
              "value": "no"
            }
            ]
          }]
        }
      }
    };
  }
};

const CheckContentusefull = async (event) => {
  if (event.currentIntent.slots.isContentUsefull === 'yes') {
    return {
      "dialogAction": {
        "type": "ElicitSlot",
        "message": {
          "contentType": "PlainText",
          "content": "Great! Is there anything else we can help you with?"
        },
        "intentName": "NeedOtherHelp",
        "slots": {
          "otherHelpInput": null
        },
        "slotToElicit": "otherHelpInput",
        "responseCard": {
          "version": 1,
          "contentType": "application/vnd.amazonaws.card.generic",
          "genericAttachments": [{
            "title": "Great! Is there anything else we can help you with?",
            "subTitle": "Great! Is there anything else we can help you with?",
            "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
            "attachmentLinkUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
            "buttons": [{
              "text": "Yes",
              "value": "yes"
            },
            {
              "text": "No",
              "value": "no"
            }
            ]
          }]
        }
      }
    };
  }
  else {
    return {
      "dialogAction": {
        "type": "ElicitSlot",
        "message": {
          "contentType": "PlainText",
          "content": "Hello, what information can we help you find?"
        },
        "intentName": "SpecificContentSearch",
        "slots": {
          "userInput": null,
          "allSearch": "yes"
        },
        "slotToElicit": "userInput",
        "responseCard": {
          "version": 1,
          "contentType": "application/vnd.amazonaws.card.generic",
          "genericAttachments": [{
            "title": "Okay, let's narrow your search further.",
            "subTitle": "Can you share more details about what you're looking for?",
            "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
            "attachmentLinkUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg"
          }]
        }
      }
    };
  }
};

const handleConsultant = async (event) => {
  return checkPreviousWork(event);
};

const handleLegalConsultancy = async (event) => {
  return sendLegalInfo(event);
};

const handleComplaint = async (event) => {
  return {
    "dialogAction": {
      "type": "ElicitSlot",
      "message": {
        "contentType": "PlainText",
        "content": "Report a complaint/speak to someone about a service issue?"
      },
      "intentName": "ReportProblem",
      "slots": {
        "reportProblemSelection": null
      },
      "slotToElicit": "reportProblemSelection",
      "responseCard": {
        "version": 1,
        "contentType": "application/vnd.amazonaws.card.generic",
        "genericAttachments": [{
          "title": "Report a complaint/speak to someone about a service issue?",
          "subTitle": "Report a complaint/speak to someone about a service issue?",
          "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
          "attachmentLinkUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
          "buttons": [{
            "text": "Yes",
            "value": "yes"
          },
          {
            "text": "No",
            "value": "no"
          }
          ]
        }]
      }
    }
  }
};

module.exports = {
  getInTouchWithInternalUser,
  checkForPreviousWork,
  allContentSearch,
  contactShipping,
  searchNameDepartment,
  findSpecific,
  adviceSelection,
  needLegalInfo,
  needOtherHelp,
  reportProblem,
  NameDepartmentSelection,
  specificContentSearch,
  CheckContentusefull,
  selecteIntent,
  handleConsultant,
  handleLegalConsultancy,
  handleComplaint,
}

function handleConfirmation(event) {
  const customMessage = [{
    type: 'PlainText',
    value: 'Thank you, we have everything we need.'
  }, {
    type: 'PlainText',
    value: 'We will forward your information to the relevant team and someone will be in touch soon (should we commit to time frame).'
  }, {
    type: 'PlainText',
    value: 'Thank you for getting in touch.'
  }];
  if (!event.currentIntent.confirmationStatus || event.currentIntent.confirmationStatus === "None") {
    return {
      "dialogAction": {
        "type": "ConfirmIntent",
        "message": {
          "contentType": "PlainText",
          "content": "Great! Before we forward this to the right team member, can you tell us if you have considered how to fund your legal/consultancy matter?"
        },
        "intentName": "CheckPreviousWork",
        "slots": {
          ...event.currentIntent.slots
        },
        "responseCard": {
          "version": 1,
          "contentType": "application/vnd.amazonaws.card.generic",
          "genericAttachments": [{
            "title": "Great! Before we forward this to the right team member",
            "subTitle": "if you have considered how to fund your legal/consultancy matter?",
            "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
            "attachmentLinkUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
            "buttons": [{
              "text": "Yes",
              "value": "yes"
            },
            {
              "text": "No. That would be all for now.",
              "value": "no"
            }
            ]
          }]
        }
      }
    }
  }
  if (event.currentIntent.confirmationStatus === "Denied") {
    customMessage.unshift({
      type: 'PlainText',
      value: 'Not a problem. Our advisers will be able to guide you through your options.'
    });
  }
  // TODO: add data in db (event.currentIntent.slots)
  return sendFullfilledMessage(JSON.stringify({ messages: customMessage }), 'CustomPayload');
}

function sendLegalInfo(event) {
  return {
    dialogAction: {
      "type": "ElicitSlot",
      "message": {
        "contentType": "PlainText",
        "content": "Are you also looking to get legal or consultancy advice?"
      },
      "intentName": "LegalInformation",
      "slots": {
        needLegalInfo: "null"
      },
      "slotToElicit": "needLegalInfo",
      "responseCard": {
        "version": 1,
        "contentType": "application/vnd.amazonaws.card.generic",
        "genericAttachments": [{
          "title": "Are you also looking to get legal or consultancy advice?",
          "subTitle": "Are you also looking to get legal or consultancy advice?",
          "imageUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
          "attachmentLinkUrl": "https://image.shutterstock.com/image-vector/dots-letter-c-logo-design-260nw-551769190.jpg",
          "buttons": [{
            "text": "Yes",
            "value": "yes"
          },
          {
            "text": "No. That would be all for now.",
            "value": "no"
          },
          {
            "text": "I'm unsure. I would like to consult someone first.",
            "value": "yes"
          }
          ]
        }]
      }
    }
  };
}

function sendFullfilledMessage(content, contentType) {
  return {
    "dialogAction": {
      "type": "Close",
      "fulfillmentState": "Fulfilled",
      "message": {
        contentType,
        content
      }
    }
  }
};

const checkPreviousWork = async (event) => {
  return {
    "dialogAction": {
      "type": "ElicitSlot",
      "message": {
        "contentType": "PlainText",
        "content": "Have you worked with us before? Let us help you connect with the right team member"
      },
      "intentName": "CheckPreviousWork",
      "slots": {
        "previouslyWorked": null,
        "userName": null,
      },
      "slotToElicit": "previouslyWorked"
    }
  }
};