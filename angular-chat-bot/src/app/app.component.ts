import { Component, OnInit } from '@angular/core';
import Auth from '@aws-amplify/auth';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  termsAccepted: boolean;
  user: any;

  async ngOnInit(): Promise<void> {
    this.user = await Auth.currentUserInfo();
    this.setLocalStorage(!!this.user)
    this.termsAccepted = localStorage.getItem('privacyAccepted') === 'true';
  }

  async setLocalStorage($event): Promise<void> {
    this.termsAccepted = $event;
    localStorage.setItem('privacyAccepted', $event);
    if (!this.user) {
      try {
        this.user = await Auth.signIn('kinjalmodi1995@gmail.com', 'kinjal1909');
      } catch (error) {
        console.log(error);
      }
    }
  }
}
